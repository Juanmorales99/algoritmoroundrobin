package com.example.roundrobin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText quantum, t1, t2, t3, t4, t5;
    String tpq, tp1, tp2, tp3, tp4, tp5;
    Button boton;
    int tQuantum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boton = findViewById(R.id.btnSimular);
    }
    public void Simular(View view) {

        int numeroQuantum,rafaga1, rafaga2, rafaga3, rafaga4, rafaga5;
        quantum = findViewById(R.id.txtQuantum);
        t1 = findViewById(R.id.txtProc1);
        t2 = findViewById(R.id.txtProc2);
        t3 = findViewById(R.id.txtProc3);
        t4 = findViewById(R.id.txtProc4);
        t5 = findViewById(R.id.txtProc5);
        tpq=quantum.getText().toString();
        tp1=t1.getText().toString();
        tp2=t2.getText().toString();
        tp3=t3.getText().toString();
        tp4=t4.getText().toString();
        tp5=t5.getText().toString();

        numeroQuantum = Integer.parseInt(tpq);
        rafaga1 =Integer.parseInt(tp1);
        rafaga2 =Integer.parseInt(tp2);
        rafaga3 =Integer.parseInt(tp3);
        rafaga4 =Integer.parseInt(tp4);
        rafaga5 =Integer.parseInt(tp5);

        Intent intent = new Intent(this, simulacion.class);
        intent.putExtra("Quantum", tpq);
        intent.putExtra("rafaga1", tp1);
        intent.putExtra("rafaga2", tp2);
        intent.putExtra("rafaga3", tp3);
        intent.putExtra("rafaga4", tp4);
        intent.putExtra("rafaga5", tp5);
        startActivity(intent);
        finish();
   }
}