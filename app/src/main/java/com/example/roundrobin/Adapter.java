package com.example.roundrobin;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import java.util.List;

public class Adapter extends ArrayAdapter<String> {

    private List<String> listColor ;
    private  Context context;
    private int resource;

    public Adapter(@NonNull Context context, int resource, List<String> listColor) {
        super(context, resource, listColor);

        this.context = context;
        this.resource = resource;
        this.listColor = listColor ;

    }
    @Override
    public View getView(int position, View v, ViewGroup parent)
    {
        View mView = v ;
        if(mView == null){
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mView = vi.inflate(resource, null);
        }

        TextView textView = (TextView) mView.findViewById(R.id.texto);
        if(listColor.get(position) != null  )
        {
            textView.setText("Tiempo rafaga proceso "+(Integer.parseInt(listColor.get(position))+1));
            textView.setTextColor(Color.BLACK);
            if(listColor.get(position).equals("0")){
            System.out.println("Hola mundo"+listColor.get(position));
            textView.setBackgroundColor(Color.rgb(255,255,0));

            } else if (listColor.get(position).equals("1"))
             {
                 textView.setBackgroundColor(Color.rgb(146,208,80));
             }else if(listColor.get(position).equals("2")){
                textView.setBackgroundColor(Color.rgb(0,176,80));
             }else if(listColor.get(position).equals("3")){
                textView.setBackgroundColor(Color.rgb(0,176,240));
            }else if(listColor.get(position).equals("4")){
                textView.setBackgroundColor(Color.rgb(128,96,0));
            }
        }

        return mView;
    }


}
