package com.example.roundrobin;
import androidx.appcompat.app.AppCompatActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;

public class simulacion extends AppCompatActivity {

    ListView listCola, listEntrada, listRta, listColor;
    TextView promEntrada, promRta;
    int quantumint, entrada=0;
    int promedioRta, promedioEntrada, sumaEntrada, num1, num2, num3, num4, num5, contador, total;

    int arrayTRafaga[], arrayFinal[], arrayEspera[], arrayTEntrada[], arrayTEntrada1[], arrayTRta[], arrayC[];
    ArrayList<String> arrayList = new ArrayList<String>();
    ArrayList<String> arrayList1 = new ArrayList<String>();
    ArrayList<String> arrayList2 = new ArrayList<String>();
    ArrayList<String> arrayList3 = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulacion);
        listCola = (ListView) findViewById(R.id.listCola);
        promEntrada=(TextView) findViewById(R.id.promEntrada);
        promRta=(TextView) findViewById(R.id.promRta);

        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        listCola.setAdapter(adapter);

        listEntrada = (ListView) findViewById(R.id.listEntrada);
        ArrayAdapter adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList1);
        listEntrada.setAdapter(adapter1);

        listRta = (ListView) findViewById(R.id.listRta);
        ArrayAdapter adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList2);
        listRta.setAdapter(adapter2);

        listColor = (ListView) findViewById(R.id.listColor);
        Adapter listAdapter = new Adapter(simulacion.this , R.layout.activity_lista, arrayList3);
        listColor.setAdapter(listAdapter);

    String quantumstring = getIntent().getStringExtra("Quantum");
    String tp1 = getIntent().getStringExtra("rafaga1");
    String tp2 = getIntent().getStringExtra("rafaga2");
    String tp3 = getIntent().getStringExtra("rafaga3");
    String tp4 = getIntent().getStringExtra("rafaga4");
    String tp5 = getIntent().getStringExtra("rafaga5");

        num1=Integer.parseInt(tp1);
        num2=Integer.parseInt(tp2);
        num3=Integer.parseInt(tp3);
        num4=Integer.parseInt(tp4);
        num5=Integer.parseInt(tp5);
        quantumint=Integer.parseInt(quantumstring);
        total=num1+num2+num3+num4+num5;

        arrayTRafaga = new int[]{num1, num2, num3, num4, num5};
        arrayFinal = new int[5];
        arrayEspera = new int[5];
        arrayTEntrada = new int[5];
        arrayTRta = new int[5];
        arrayTRafaga = new int[]{num1, num2, num3, num4, num5};
        arrayFinal = new int[5];
        arrayEspera = new int[5];
        arrayTEntrada = new int[5];
        arrayTEntrada1 = new int[5];
        arrayTRta = new int[5];
        arrayC= new int[total];
        contador = 0;

        int j=1,k=0,aux=0,g=0;
        for (int b=0;b<5;b++){
            arrayTEntrada[b]=0;
            arrayTEntrada1[b]=0;
            arrayTRta[b]=0;	}
        do{
            for (int i = 0; i < 5; i++) {

                if(arrayTRafaga[i]==0){
                    if(aux==0)
                        arrayTEntrada[i]=0;
                }
                else{
                    if(aux==0)
                        arrayTEntrada[i]+=entrada;
                    if(aux>0 && aux<2)
                        arrayTEntrada1[i]=contador-arrayTEntrada1[i]+1;
                    if(arrayTEntrada[i]!=0) {
                        if(aux==0)
                            arrayTEntrada[i]=entrada;
                    }
                    if (arrayTRafaga[i] >= quantumint) {
                        arrayTRafaga[i] = arrayTRafaga[i] - quantumint;//haga la resta entre rafaga y proceso
                        contador += (quantumint);//inicio el contador que me dira el tiempo transcurrido
                        entrada+=quantumint;

                    }
                    else {
                        contador += arrayTRafaga[i];
                        entrada+=arrayTRafaga[i];
                        arrayTRafaga[i] = 0;
                    }
                    if(i==0) {
                        do {
                            arrayC[g]=0;
                            g++;
                        } while(g<contador);

                    }else if(i==1) {
                        do {
                            arrayC[g]=1;
                            g++;
                        } while(g<contador);
                    }else if(i==2) {
                        do {
                            arrayC[g]=2;
                            g++;
                        } while(g<contador);
                    }else if(i==3) {
                        do {
                            arrayC[g]=3;
                            g++;
                        } while(g<contador);
                    }else if(i==4) {
                        do {
                            arrayC[g]=4;
                            g++;
                        } while(g<contador);
                    }

                if (arrayTRafaga[i] == 0) {
                        arrayFinal[i] = j;
                        j++;
                        arrayEspera[i] = 0;
                        k++;
                    } else {
                        arrayEspera[i] = arrayTRafaga[i];
                        k++;
                    }
                    if(arrayTRafaga[i]==0)
                        arrayTRta[i] = contador-1;

                    if(aux==0 && arrayTRafaga[i]!=0 &&arrayEspera[i]!=0)
                        arrayTEntrada1[i]=contador;

                }
            }
            aux++;

            for (int s : arrayEspera) {
                arrayList.add(String.valueOf(s));
            }

        }while(arrayTRafaga[0]+arrayTRafaga[1]+arrayTRafaga[2]+arrayTRafaga[3]+arrayTRafaga[4]!=0);
        for(int i=0;i<5;i++){
            arrayTEntrada[i]+=arrayTEntrada1[i];
            promedioRta+=arrayTRta[i];
            sumaEntrada+=arrayTEntrada[i];
        }
        promedioRta/=5;
        promedioEntrada/=5;

     //   promEntrada.setText(promedioEntrada/=5);

        for(int t:arrayTEntrada){
            arrayList1.add(String.valueOf(t));
        }
        for(int u:arrayTRta){
            arrayList2.add(String.valueOf(u));
            
        }
        for(int u:arrayC) {
            arrayList3.add(String.valueOf(u));
        }
}}